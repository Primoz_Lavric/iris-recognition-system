import shlex
from subprocess import Popen, PIPE
import RPi.GPIO as GPIO
import time

def executeAndWait(cmd):
    process = Popen(shlex.split(cmd), stdout=PIPE)
    process.communicate()
    return process.wait()

def whiteLedBlink():
    executeAndWait("i2cset -y 1 0x70 0x00 0x5a")
    time.sleep(0.5)
    executeAndWait("i2cset -y 1 0x70 0x00 0x00")

def captureImage():
    executeAndWait("raspistill -sh 50 -o captured.jpg")
    
def irisRecognition():
    status = executeAndWait("irisRecog authenticate ./captured.jpg")

    # Two blinks for acceptance and one blink for rejection
    if status == 0:
        whiteLedBlink()
        time.sleep(0.5)
        whiteLedBlink()
    else
        whiteLedBlink()

def turnOnLeds():
    executeAndWait("i2cset -y 1 0x70 0x00 0xa5")

def turnOffLeds():
    executeAndWait("i2cset -y 1 0x70 0x00 0x00")

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while True:
    input_state = GPIO.input(18)
    # Check if the button was pressed
    if input_state == False:
        turnOnLeds()
        captureImage()
        irisRecognition()
        turnOffLeds()

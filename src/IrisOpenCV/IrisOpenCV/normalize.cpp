#include "normalize.h"

using namespace cv;

Mat normalizeIris(Mat &irisImage, Vec<double, 3> &pupil, Vec<double, 3> &iris, int magnitude, int padding, int nR, int nTheta) {

	// Use pupil as center
	Point2f center(pupil[0], pupil[1]);
	float pupilRadius = pupil[2];
	// Subtract second norm of vector between iris and pupil center from the iris radius
	float irisRadius = iris[2] - sqrt(pow(pupil[0] - iris[0], 2) + pow(pupil[1] - iris[1], 2));

	// Convert to polar
	Mat polarIris;
	cv::logPolar(irisImage, polarIris, center, magnitude, INTER_LINEAR);

	// Fetch the iris part
	Mat croppedIris;
	cv::transpose(polarIris(Rect(magnitude * log(pupilRadius + padding), 0, magnitude * (log(irisRadius) - log(pupilRadius + padding)), polarIris.rows)), croppedIris);
	polarIris.release();

	Mat resizedIris;
	cv::resize(croppedIris, resizedIris, cv::Size(nTheta, nR));


	return resizedIris;
}

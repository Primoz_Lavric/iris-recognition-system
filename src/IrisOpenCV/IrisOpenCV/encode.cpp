#include "encode.h"

using namespace cv;
 
Mat encodePolarIris(Mat &polarImage, float wavelength, float filterSigma) {
	Mat phase = convolveLogGabor(polarImage, wavelength, filterSigma);

	Mat phaseParts[2];
	split(phase, phaseParts);
	Mat phaseR = phaseParts[0];
	Mat phaseI = phaseParts[1];

	int codeDim[2] = { polarImage.rows, polarImage.cols * 2 };
	
	// Create iris code matrix
	Mat irisCode(2, codeDim, CV_8U);

	for (int i = 0; i < polarImage.rows; i++) {
		for (int j = 0; j < polarImage.cols; j++) {
			irisCode.at<char>(i, j * 2) = (phaseR.at<float>(i, j) > 0.0f) ? 1 : 0;
			irisCode.at<char>(i, j * 2 + 1) = (phaseI.at<float>(i, j) > 0.0f) ? 1 : 0;
		}
	}

	return irisCode;
}

Mat convolveLogGabor(Mat &polarImage, float wavelength, float filterSigma) {

	int cols = polarImage.cols;
	int rows = polarImage.rows;

	// Construct logGabor filter
	Mat logGaborFilter(1, cols, CV_32F, 0.0f);

	for (float i = 0; i < cols /2 + 1; i++) {
		logGaborFilter.at<float>(i) = exp(- powf(log(i / polarImage.cols * wavelength), 2)) / (2 * powf(log(filterSigma), 2));
	}
	
	Mat fDomainRow, filteredRow, floatPolar;

	Mat phase(polarImage.size(), CV_32FC2);

	// dft requires float input
	polarImage.convertTo(floatPolar, CV_32F);

	for (int i = 0; i < rows; i++) {
		dft(floatPolar.row(i), fDomainRow, DFT_COMPLEX_OUTPUT);

		for (int j = 0; j < cols; j++) {
			fDomainRow.at<Vec2f>(j) *= logGaborFilter.at<float>(j);
		}

		// Inverse furier transform
		idft(fDomainRow, filteredRow);

		filteredRow.copyTo(phase.row(i));
	}

	return phase;
}


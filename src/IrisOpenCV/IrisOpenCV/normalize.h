#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

cv::Mat normalizeIris(cv::Mat &irisImage, cv::Vec<double, 3> &pupil, cv::Vec<double, 3> &iris, int magnitude, int padding, int nR, int nTheta);

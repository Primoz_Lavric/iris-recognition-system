#pragma once
#define _USE_MATH_DEFINES

#include <algorithm>
#include <cmath>
#include <opencv2/core.hpp>
#include <iostream>

using namespace std;

class DaugmanIntegroDifferential {
public:

	static pair<cv::Vec<double, 3>, cv::Vec<double, 3>> findIrisBoundaries(cv::Mat &irisImage);


private:
	// Returns inner boundary of iris
	static cv::Vec<double, 3> irisInnerBoundary(cv::Mat &irisImage);

	// Returns outer boundary of iris
	static cv::Vec<double, 3> irisOuterBoundary(cv::Mat &irisImage, cv::Vec<double, 3> innerBoundary);

	// Calculates contour (circural) integral for the given bounding circle using discrete Reimann approach
	static double countourCircularIntegral(cv::Mat &irisImage, cv::Vec<double, 3> circle, double precision);

	// Calculates contour (circural) integral for the given bounding circle using discrete Reimann approach (ignores eyelid regions)
	static double countourCircularIntegralOuter(cv::Mat &irisImage, cv::Vec<double, 3> circle, double precision);

	// Calculates 3D blur
	static cv::Mat blurVolume(cv::Mat &volume, int kernelSize);
};


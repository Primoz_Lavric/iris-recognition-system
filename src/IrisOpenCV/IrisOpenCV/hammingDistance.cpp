#include "hammingDistance.h"

using namespace cv;

float getHammingDistance(Mat codeA, Mat maskA, Mat &codeB, Mat &maskB, int numShifts) {
	float bestHamming = 1.0f;

	Mat combinedMask, misses;
	int usedBits, numMisses;

	// Circulary shift code and mask A to starting position
	shift(codeA, codeA, Point2f(-numShifts, 0), BORDER_WRAP);
	shift(maskA, maskA, Point2f(-numShifts, 0), BORDER_WRAP);

	for (int i = 0; i < numShifts * 2; i++) {
		combinedMask = maskA & maskB;

		// Determine how many bits are used (not masked)
		usedBits = countNonZero(combinedMask);

		// XOR the codes and apply the mask
		bitwise_xor(codeA, codeB, misses);
		misses = misses & combinedMask;
		
		// Count number of misses
		numMisses = countNonZero(misses);

		// Check if the new distance is better than previous
		if (usedBits > 0) {
			float hammingDistance = ((float) numMisses) / usedBits;

			if (hammingDistance < bestHamming) {
				bestHamming = hammingDistance;
			}
		}

		// Circulary shift code and mask to the right
		shift(codeA, codeA, Point2f(1, 0), BORDER_WRAP);
		shift(maskA, maskA, Point2f(1, 0), BORDER_WRAP);
	}

	return bestHamming;
}

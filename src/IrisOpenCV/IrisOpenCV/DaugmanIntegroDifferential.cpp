#include "DaugmanIntegroDifferential.h"


pair<cv::Vec<double, 3>, cv::Vec<double, 3>> DaugmanIntegroDifferential::findIrisBoundaries(cv::Mat & irisImage)
{
	cv::Vec<double, 3> innerBoundary = DaugmanIntegroDifferential::irisInnerBoundary(irisImage);

	innerBoundary[0] = innerBoundary[0] - 1;
	innerBoundary[1] = innerBoundary[1] - 1;

	cv::Vec<double, 3> outerBoundary = DaugmanIntegroDifferential::irisOuterBoundary(irisImage, innerBoundary);

	outerBoundary[0] = outerBoundary[0] - 1;
	outerBoundary[1] = outerBoundary[1] - 1;

	return make_pair(innerBoundary, outerBoundary);
}

cv::Vec<double, 3> DaugmanIntegroDifferential::irisInnerBoundary(cv::Mat &irisImage) {
	
	int X = irisImage.cols;
	int Y = irisImage.rows;

	//--------------------------------------------------------------//
	// INTEGRODIFFERENTIAL OPERATOR COARSE (jump - level precision) //
	//--------------------------------------------------------------//
	
	// Width of the external margin for which search is excluded
	float sect = X / 4;
	float minrad = 10.0f;
	float maxrad = sect * 0.8;
	// Precision of coarse search (pixels)
	int precisionJump = 4;

	// Create hough space (y, x, r)
	int dims[3] = { floor((Y - 2 * sect) / precisionJump),
					floor((X - 2 * sect) / precisionJump),
					floor((maxrad - minrad) / precisionJump) };

	cv::Mat houghSpace(3, dims, CV_32F);

	double integrationPrecision = 1;

	// Calculate circualr integration for Hough space
	for (int y = 0; y < dims[0]; y++) {
		for (int x = 0; x < dims[1]; x++) {
			for (int r = 0; r < dims[2]; r++) {
				houghSpace.at<float>(y, x, r) = DaugmanIntegroDifferential::countourCircularIntegral(irisImage, cv::Vec<float, 3>(sect + (y + 1) * precisionJump, sect + (x + 1) * precisionJump, minrad + (r + 1) * precisionJump), integrationPrecision);
			}
		}
	}

	// Calculate partial differential R of hough space
	for (int y = 0; y < dims[0]; y++) {
		for (int x = 0; x < dims[1]; x++) {
			for (int r = dims[2] - 1; r > 0 ; r--) {
				houghSpace.at<float>(y, x, r) -= houghSpace.at<float>(y, x, r - 1);
			}

			houghSpace.at<float>(y, x, 0) = 0;
		}
	}

	// Blur Hough space
	cv::Mat houghBlurred = DaugmanIntegroDifferential::blurVolume(houghSpace, 3);
	// We dont need Hough space anymore
	houghSpace.release();

	// Find max value
	double testMaxval;
	int maxIdx[3];

	minMaxIdx(houghBlurred, 0, &testMaxval, 0, maxIdx);
	// We dont need BlurredHough space anymore
	houghBlurred.release();

	cv::Vec<double, 3> coarseCircle(sect + (maxIdx[1] + 1) * precisionJump,
		sect + (maxIdx[0] + 1) * precisionJump,
		minrad + maxIdx[2] * precisionJump);

	//--------------------------------------------------------------//
	//  INTEGRODIFFERENTIAL OPERATOR FINE (pixel-level precision)   //
	//--------------------------------------------------------------//
	
	// Initialize fine Hough space
	int dimsFine[] = { (int)(precisionJump * 2), (int)(precisionJump * 2), (int)(precisionJump * 2) };

	houghSpace = cv::Mat(3, dimsFine, CV_32F);

	// Set fine precision
	integrationPrecision = 0.1;

	// Calculate fine circualr integration for Hough space
	for (int y = 0; y < dimsFine[0]; y++) {
		for (int x = 0; x < dimsFine[1]; x++) {
			for (int r = 0; r < dimsFine[2]; r++) {
				houghSpace.at<float>(y, x, r) = DaugmanIntegroDifferential::countourCircularIntegral(irisImage, cv::Vec<float, 3>(coarseCircle[1] - precisionJump + (y + 1), coarseCircle[0] - precisionJump + (x + 1), coarseCircle[2] - precisionJump + (r + 1)), integrationPrecision);
			}
		}
	}

	// Calculate partial differential R of fine Hough space
	for (int y = 0; y < dimsFine[0]; y++) {
		for (int x = 0; x < dimsFine[1]; x++) {
			for (int r = dimsFine[2] - 1; r > 0; r--) {
				houghSpace.at<float>(y, x, r) -= houghSpace.at<float>(y, x, r - 1);
			}

			houghSpace.at<float>(y, x, 0) = 0;
		}
	}

	// Blur Hough space
	houghBlurred = DaugmanIntegroDifferential::blurVolume(houghSpace, 3);
	houghSpace.release();

	// Find max value
	minMaxIdx(houghBlurred, 0, &testMaxval, 0, maxIdx);
	// We dont need BlurredHough space anymore
	houghBlurred.release();

	

	// Return fine circle
	return cv::Vec<double, 3>(coarseCircle[1] - precisionJump + (maxIdx[1] + 1),
		coarseCircle[0] - precisionJump + (maxIdx[0] + 1),
		coarseCircle[2] - precisionJump + maxIdx[2]);
}

cv::Vec<double, 3> DaugmanIntegroDifferential::irisOuterBoundary(cv::Mat & irisImage, cv::Vec<double, 3> innerBoundary) {

	// Very maximum displacement 15 % (Daugman 2004)
	double maxDisplacement = round(innerBoundary[2] * 0.15);

	// 0.1 - 0.8 (Daugman 2004
	double minrad = round(innerBoundary[2] / 0.8);
	double maxrad = round(innerBoundary[2] / 0.3);

	// Create hough space (y, x, r)
	int dims[3] = { 2 * maxDisplacement,
		2 * maxDisplacement,
		maxrad - minrad };

	cv::Mat houghSpace(3, dims, CV_32F);

	double integrationPrecision = 0.05;

	// Calculate circular integration for Hough space
	for (int y = 0; y < dims[0]; y++) {
		for (int x = 0; x < dims[1]; x++) {
			for (int r = 0; r < dims[2]; r++) {
				houghSpace.at<float>(y, x, r) = DaugmanIntegroDifferential::countourCircularIntegralOuter(irisImage, cv::Vec<float, 3>(innerBoundary[0] - maxDisplacement + (y + 1), innerBoundary[1] - maxDisplacement + (x + 1), minrad + (r + 1)), integrationPrecision);
			}
		}
	}

	// Calculate partial differential R of hough space
	for (int y = 0; y < dims[0]; y++) {
		for (int x = 0; x < dims[1]; x++) {
			for (int r = dims[2] - 1; r > 0; r--) {
				houghSpace.at<float>(y, x, r) -= houghSpace.at<float>(y, x, r - 1);
			}

			houghSpace.at<float>(y, x, 0) = 0;
		}
	}

	// Blur Hough space
	cv::Mat houghBlurred = DaugmanIntegroDifferential::blurVolume(houghSpace, 7);
	// We dont need Hough space anymore
	houghSpace.release();

	// Find max value
	double testMaxval;
	int maxIdx[3];

	minMaxIdx(houghBlurred, 0, &testMaxval, 0, maxIdx);
	// We dont need BlurredHough space anymore
	houghBlurred.release();

	return cv::Vec<double, 3>(innerBoundary[0] - maxDisplacement + (maxIdx[0] + 1),
		innerBoundary[1] - maxDisplacement + (maxIdx[1] + 1),
		minrad + maxIdx[2]);
}

double DaugmanIntegroDifferential::countourCircularIntegralOuter(cv::Mat &irisImage, cv::Vec<double, 3> circle, double precision) {
	double sum = 0;
	int x, y;


	// Integration region, avoiding eyelids ([2/6 4/6] * pi)
	for (double ang = 2.0/6.0 * M_PI; ang < 4.0/6.0 * M_PI; ang += precision) {

		x = round(circle[0] + sin(ang) * circle[2]);
		y = round(circle[1] - cos(ang) * circle[2]);

		// Constrain x and y
		x = max(1, min(x, irisImage.cols - 1));
		y = max(1, min(y, irisImage.rows - 1));

		sum += irisImage.at<unsigned char>(y - 1, x - 1);
	}

	// Integration region, avoiding eyelids [8/6 10/6] * pi
	for (double ang = 8.0/6.0 * M_PI; ang < 10.0/6.0 * M_PI; ang += precision) {

		x = round(circle[0] + sin(ang) * circle[2]);
		y = round(circle[1] - cos(ang) * circle[2]);

		// Constrain x and y
		x = max(1, min(x, irisImage.cols - 1));
		y = max(1, min(y, irisImage.rows - 1));

		sum += irisImage.at<unsigned char>(y - 1, x - 1);
	}

	return sum;
}

double DaugmanIntegroDifferential::countourCircularIntegral(cv::Mat &irisImage, cv::Vec<double, 3> circle, double precision) {
	
	double sum = 0;
	int x, y;

	for (double ang = 0; ang < 2 * M_PI;  ang += precision) {

		x = round(circle[0] + sin(ang) * circle[2]);
		y = round(circle[1] - cos(ang) * circle[2]);

		// Constrain x and y
		x = max(1, min(x, irisImage.cols - 1));
		y = max(1, min(y, irisImage.rows - 1));

		sum += irisImage.at<unsigned char>(y-1, x-1);
	}

	return sum;
}

cv::Mat DaugmanIntegroDifferential::blurVolume(cv::Mat &volume, int kernelSize) {
	int x = volume.size[0];
	int y = volume.size[1];
	int z = volume.size[2];

	int sideSize = (kernelSize - 1) / 2;

	cv::Mat blured(3, volume.size, CV_32F);

	for (int i = 0; i < x; i++) {
		for (int j = 0; j < y; j++) {
			for (int k = 0; k < z; k++) {

				// init
				blured.at<float>(i, j, k) = 0;

				// Fetch filter bounds for current position
				int lX = max(0, min(x - 1, i - sideSize));
				int rX = max(0, min(x - 1, i + sideSize));

				int lY = max(0, min(y - 1, j - sideSize));
				int rY = max(0, min(y - 1, j + sideSize));

				int lZ = max(0, min(z - 1, k - sideSize));
				int rZ = max(0, min(z - 1, k + sideSize));

				// Apply blur filter
				for (int a = lX; a <= rX; a++) {
					for (int b = lY; b <= rY; b++) {
						for (int c = lZ; c <= rZ; c++) {
							blured.at<float>(i, j, k) += volume.at<float>(a, b, c);
						}
					}
				}

			}
		}
	}

	return blured;
}

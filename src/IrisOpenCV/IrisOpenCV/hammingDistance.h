#pragma once

#ifndef HAMMING_DISTANCE
#define HAMMING_DISTANCE

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "shift.hpp"

float getHammingDistance(cv::Mat codeA, cv::Mat maskA, cv::Mat &codeB, cv::Mat &maskB, int numShifts);

#endif
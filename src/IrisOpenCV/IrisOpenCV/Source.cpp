#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include "DaugmanIntegroDifferential.h"
#include "normalize.h"
#include "encode.h"
#include "hammingDistance.h"
#include <string>
#include <chrono>
#include <iomanip>
#include <ctime>

#ifdef __linux__
#include <sys/stat.h>
#include <dirent.h> 
#include <stdio.h> 
#elif _WIN32
#include <windows.h>
#endif

//#define DEBUG

using namespace cv;
using namespace std;

string usage = "Usage:\n\tscript enroll imagepath\tEnrolls image in the database.\n\tscript authenticate imagepath\tTries to authenticate the input iris.";

// Pupil specular reflection
#define PUPIL_THRESHOLD 50

// Normalization
#define R_SAMPLES 32
#define THETA_SAMPLES 256

// Hamming distance comparison
#define NUM_SHIFTS 18

// Encoding
#define WAVELENGTH 18
#define SMOOTHING_SIGMA 0.5

#define MODE_ENROLL 0
#define MODE_AUTHENTICATE 1

// Acceptance threshold
#define ACCEPTANCE_THRESHOLD 0.43

int main(int argc, char** argv)
{
	// Check if enough input argument
	if (argc != 3) {
		cout << usage << endl;
		return -1;
	}

	// Determine the execution mode
	int mode = MODE_AUTHENTICATE;

	if (strcmp("enroll", argv[1]) == 0) {
		mode = MODE_ENROLL;
	}
	else if (strcmp("authenticate", argv[1]) != 0) {
		cout << usage << endl;
		return -1;
	}


	// Try to read the input image
	Mat irisImage;
	irisImage = imread(argv[2], CV_LOAD_IMAGE_GRAYSCALE);

	imwrite("gray.jpg", irisImage);

	if (!irisImage.data) {
		cout << "Could not open or find the iris image" << std::endl;
		return -1;
	}

	auto startTime = chrono::high_resolution_clock::now();

	// -------------------------------
	// REMOVAL OF SPECULAR REFLECTIONS
	// -------------------------------

	// Init pupil mask
	Mat pupil = Mat::zeros(irisImage.size(), CV_8U);

	int avg = 0;
	int count = 0;

	// Fetch pupil area. (pixels with intensity lower than 50) and also calculate average intensity of pupil pixels
	for (int i = 0; i < irisImage.rows; i++) {
		for (int j = 0; j < irisImage.cols; j++) {
			if (irisImage.at<unsigned char>(i, j) < PUPIL_THRESHOLD) {
				pupil.at<unsigned char>(i, j) = 1;
				avg += irisImage.at<unsigned char>(i, j);
				count++;
			}
		}
	}

	// Average used for filling the specular reflection
	avg /= count;

	// Use morphological operations to fill specular reflection holes
	Mat pupilDilated;
	dilate(pupil, pupilDilated, Mat(), Point(-1, -1), 6, 1, 1);
	erode(pupilDilated, pupil, Mat(), Point(-1, -1), 6, 1, 1);

	// Fill specular in the original image with average pupil intensity
	for (int i = 0; i < pupil.rows; i++) {
		for (int j = 0; j < pupil.cols; j++) {
			if (pupil.at<unsigned char>(i, j) > 0 && irisImage.at<unsigned char>(i, j) > 30) {
				irisImage.at<unsigned char>(i, j) = avg;
			}
		}
	}


	// ----------------------------------------------------
	// IRIS SEGMENTATION USING DAUGMAN INTEGRO DIFFERENTIAL
	// ----------------------------------------------------
	pair<Vec<double, 3>, Vec<double, 3>> irisBoundaries = DaugmanIntegroDifferential::findIrisBoundaries(irisImage);

	// ----------------------------------------------------
	// IRIS NORMALIZATION AND ENCODING
	// ----------------------------------------------------
	Mat polarIris = normalizeIris(irisImage, irisBoundaries.first, irisBoundaries.second, 40, 6, R_SAMPLES, THETA_SAMPLES);
	Mat irisCode = encodePolarIris(polarIris, WAVELENGTH, SMOOTHING_SIGMA);

	// Supervised enrollment
	if (mode == MODE_ENROLL) {
		
		// Draw circles that represent iris boundaries
		cv::circle(irisImage,
			cv::Point(irisBoundaries.first[0] - 1, irisBoundaries.first[1] - 1),
			irisBoundaries.first[2],
			Scalar(255, 0, 255),
			1.2,
			3);

		cv::circle(irisImage,
			cv::Point(irisBoundaries.second[0], irisBoundaries.second[1]),
			irisBoundaries.second[2],
			Scalar(255, 0, 255),
			1.2,
			3);

		// Display iris image with boundaries
		cv::namedWindow("Segmentation", WINDOW_AUTOSIZE);
		cv::imshow("Segmentation", irisImage);

		// Display polar representation of iris
		cv::namedWindow("Polar", WINDOW_AUTOSIZE);
		cv::imshow("Polar", polarIris);

		// Display user code
		cv::namedWindow("Iris code", WINDOW_AUTOSIZE);
		cv::imshow("Iris code", irisCode * 255);

		// Ask user if he/she wants to enroll the image
		cout << "Should this Iris be enrolled in the database? (y/n) ";
		char input = waitKey();

		if (input != 'y' && input != 'Y') {
			cout << "User terminated iris enrollment!";
			return -1;
		}

#ifdef _WIN32
		CreateDirectory(".\\database\\", NULL);
#elif __linux_
		int status = mkdir("database", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		if (status == -1) {
			cout << "Failed to create database directory." << endl;
			return -1;
		}
#endif

		// Store iris code with current timestamp
		auto t = time(nullptr);
		auto tm = *localtime(&t);
		ostringstream oss;
		oss << put_time(&tm, "%d-%m-%Y_%H-%M-%S");
		auto timestampStr = oss.str();

		// Store iris code
		imwrite("database/" + timestampStr + ".jpg", irisCode);
	}
	else if (mode == MODE_AUTHENTICATE) {

		Mat emptyMask(irisCode.size(), CV_8U);
		emptyMask = 1;

		float bestHamming = 1;
		Mat irisCodeDB;

#ifdef _WIN32
		HANDLE hFind;
		WIN32_FIND_DATA data;

		// Iterate through the images in the database
		hFind = FindFirstFile(".\\database\\*.jpg", &data);
		if (hFind != INVALID_HANDLE_VALUE) {
			do {
				string filename = data.cFileName;
				irisCodeDB = imread("./database/" + filename, CV_LOAD_IMAGE_GRAYSCALE);

				// Compute hamming distance between DB iris code and current iris code
				float newHamming = getHammingDistance(irisCode, emptyMask, irisCodeDB, emptyMask, NUM_SHIFTS);
				
				// Store the best one
				if (newHamming < bestHamming) {
					bestHamming = newHamming;
				}

			} while (FindNextFile(hFind, &data));
			FindClose(hFind);
		}
#elif __linux_
		DIR           *d;
		struct dirent *dir;
		d = opendir("./database");

		if (d) {
			while ((dir = readdir(d)) != NULL) {
				string filename = dir->d_name;
				irisCodeDB = imread("./database/" + filename, CV_LOAD_IMAGE_GRAYSCALE);
			}

			// Compute hamming distance between DB iris code and current iris code
			float newHamming = getHammingDistance(irisCode, emptyMask, irisCodeDB, emptyMask, 9);

			// Store the best one
			if (newHamming < bestHamming) {
				bestHamming = newHamming;
			}

			closedir(d);
		}
#endif

		// Terminate the program with 0 if the iris is accepted otherwise terminate it with 1
		if (mode == MODE_AUTHENTICATE) {
			if (bestHamming < ACCEPTANCE_THRESHOLD) {
				return 0;
			}
			else {
				return 1;
			}
		}
	}

#ifdef DEBUG
	// EXPERIMENTAL
	Mat blurred;
	GaussianBlur(irisImage, blurred, Size(9, 9), 0, 0);

	namedWindow("blurred", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("blurred", blurred); // Show our image inside it.

	Mat edges;

	Canny(blurred, edges, 10, 30, 3);

	namedWindow("Edges", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("Edges", edges * 255); // Show our image inside it.


	cv::circle(irisImage,
		cv::Point(irisBoundaries.first[0]-1, irisBoundaries.first[1]-1),
		irisBoundaries.first[2],
		Scalar(255, 255, 255),
		1,
		3);


	cv::circle(irisImage,
		cv::Point(irisBoundaries.second[0], irisBoundaries.second[1]),
		irisBoundaries.second[2],
		Scalar(255, 255, 255),
		1,
		3);


	namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("Display window", irisImage); // Show our image inside it.
	
	namedWindow("Polar", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("Polar", polarIris); // Show our image inside it.

	namedWindow("IrisCode", WINDOW_AUTOSIZE); // Create a window for display.
	imshow("IrisCode", irisCode * 255); // Show our image inside it.

	waitKey();
#endif // DEBUG

	auto endTime = chrono::high_resolution_clock::now();
	cout << "Execution time: " << chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count() << "ms";

	return 0;
}
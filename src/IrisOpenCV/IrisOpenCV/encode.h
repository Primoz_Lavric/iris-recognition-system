#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

cv::Mat encodePolarIris(cv::Mat &polarImage, float wavelength, float filterSigma);

cv::Mat convolveLogGabor(cv::Mat &polarImage, float wavelength, float filterSigma);